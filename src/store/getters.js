export function currentRoom(state) {
  return state.currentRoomId ? state.rooms[state.currentRoomId - 1] : {}
}

export function currentMessages(state) {
  const room = currentRoom(state)
  return room.messages ? room.messages.map(id => state.messages[id - 1]) : []
}

export function currentUser(state) {
	return state.auth.user
}

export function currentTeam(state) {
	return state.currentTeamId ? state.teams[state.currentTeamId - 1] : {}
}

export function rooms(state) {
  return state.rooms ? state.rooms : []
  //.map(obj => obj.is_dm)
}
