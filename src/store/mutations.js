import { set } from 'vue'
import * as types from './mutation-types'
var jwt = require('jwt-decode');
import router from '../router'

export default {
  [types.RECEIVE_MESSAGE] (state, message) {
    addMessage(state, message)
  },
  [types.RECEIVE_ASSET] (state, asset) {
    setAsset(state, asset)
  },
  [types.SWITCH_ROOM] (state, id) {
    setCurrentRoom(state, id)
  },
  [types.USER_LOGGED_IN] (state, user){
    setCurrentUser(state, user)
    router.push('messages')
  }
}

// individual adders

function addTeam (state, team) {
  set(state.teams, team.id - 1, team)
}

function addRoom (state, room) {
  room.messages = []
  room.lastMessage = null
  set(state.rooms, room.id - 1, room)
}

function addMessage (state, message) {
  message.isRead = message.room_id === state.currentRoomId
  const room = state.rooms[message.room_id - 1]
  if (!room.messages.some(id => id === message.id)) {
    room.messages.push(message.id)
    room.lastMessage = message
  }
  set(state.messages, message.id - 1, message)
}

// property setters

function setCurrentRoom (state, id) {
  state.currentRoomId = id
  state.rooms[id - 1].lastMessage.isRead = true
}

function setCurrentTeam (state, id) {
  state.currentTeamId = id
}

function setCurrentUser (state) {
  let payload = jwt(localStorage.getItem('vmbtoken'))
  let decodedUser = {
    id : payload.sub,
    firstName : payload.first_name,
    lastName : payload.last_name
  }
  state.auth.user = decodedUser
  state.auth.authenticated = true
}

// full asset setter

function setAsset (state, asset) {
  setRooms(state, asset.rooms)
  setMessages(state, asset.messages)
  setTeams(state, asset.teams)
  setCurrentRoom(state, 1)
  setCurrentTeam(state, asset.team.id)
}

//collection setters

function setRooms (state, rooms){
  rooms.forEach(room => {
    addRoom(state, room)
  })
}

function setMessages (state, messages){
  messages.forEach(message => {
    addMessage(state, message)
  })
}

function setTeams (state, teams){
  teams.forEach(team => {
    addTeam(state, team)
  })
}
