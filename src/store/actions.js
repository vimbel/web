import * as api from '../api'
import * as types from './mutation-types'
import io from 'socket.io-client'
const msocket = io('http://' + window.location.hostname + ':8890')

export const loginUser = ({ commit }, credentials) => {
	api.loginUser({ credentials }, response => {
		commit(types.USER_LOGGED_IN, response)
	})
}

export const getUserAsset = ({ commit }, { userId, teamId }) => {
	api.getUserAsset({ userId, teamId }, asset => {
		commit(types.RECEIVE_ASSET, asset)
	})
}

export const getRoomMessages = ({ commit }, roomId) => {
	api.getUserMessages({ userId }, messages => {
		commit(types.RECEIVE_MESSAGES, messages)
	})
}

export const sendMessage = ({ commit }, { text, roomId, userId }) => {
	api.createMessage({ text, roomId, userId }, message => {
		commit(types.RECEIVE_MESSAGE, message)
		msocket.emit('push-message', message)
	})
}

export const addMessageFromSocket = ({ commit }, message) => {
	commit(types.RECEIVE_MESSAGE, message)
}

export const switchRoom = ({ commit }, id) => {
	commit(types.SWITCH_ROOM, id)
}
