import Vue from 'vue'
const ENDPOINT = 'http://' + window.location.hostname + ':8000/api/'

export function loginUser ({ credentials }, callback) {
  Vue.http.post(ENDPOINT + 'auth/login', credentials).then((response) => {
    localStorage.setItem('vmbtoken', response.data)
    callback(response.data)
  }, (response) => {
    //error
  })
}

export function getUserAsset ({ userId, teamId }, callback) {
  const payload = {
    user_id : userId,
    team_id : teamId
  }

  Vue.http.post(ENDPOINT + 'assets/web', payload).then((response) => {
    callback(response.data)
  }, (response) => {
      //error
  })
}

export function createMessage ({ text, roomId, userId }, callback) {
  const message = {
    room_id : roomId,
    user_id : userId,
    content : text
  }

  Vue.http.post(ENDPOINT + 'messages/', message).then((response) => {
    callback(response.data)
  }, (response) => {
    //error
  })
}
