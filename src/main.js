import Vue from 'vue'
import App from './App.vue'
import Resource from 'vue-resource'
import { sync } from 'vuex-router-sync'
import store from './store'
import router from './router'

Vue.use(Resource)

Vue.config.debug = true
Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('vmbtoken')

Vue.filter('time', timestamp => {
	return new Date(timestamp).toLocaleTimeString()
})

const app = new Vue({ router, store, ...App }).$mount('app')

//sync(store, router)
