import Vue from 'vue'
import Router from 'vue-router'

import Home from '../components/Home'
import Chat from '../components/Chat'

Vue.use(Router)

const routes = [
  { path: '', component: Home },
  { path: '/messages', component: Chat,
    children: [

    ]
  }
]

export default new Router({
  mode: 'history',
  routes: routes
})
